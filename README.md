# App con lista de Paises
> Imágenes de la ejecución


![]( app/src/main/res/drawable/ejecucion.jpeg)

![]( app/src/main/res/drawable/fragmentomapa.jpeg)

![]( app/src/main/res/drawable/takefoto.jfif)

## Descripción

La app está estructurada con recycler view  donde contiene videos del himno nacional de los paises  (WebView), imagenes de las banderas (ImageView) y los nombres de los país con su capital y población (TextView). Se creo tres clases una de pais modelo que contiene los getter y setter para poder llamarlo en otra clase, tambien una que se llama RecyclerViewAdaptador que va hacer como un adaptador y en el Main que creo una list para poder obtener todo de las otras clases y asi poderlo presentar en la app.


