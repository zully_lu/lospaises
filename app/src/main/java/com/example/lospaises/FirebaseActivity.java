package com.example.lospaises;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.Map;

public class FirebaseActivity extends AppCompatActivity {


    private FirebaseFirestore db;
    private CollectionReference crAmazon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firebase);
        db = FirebaseFirestore.getInstance();
        crAmazon = db.collection("paises");
    }
    public void onClickCrearPaises(View view) {
        Map<String,Object> pais = new HashMap<>();
        pais.put("pais","Zimbabue");
        pais.put("poblacion","18.56.789");
        pais.put("video","https://www.youtube.com/watch?v=bGMgUIvuoKs");
        pais.put("imagen","Zimbabue");

        db.collection("paises")
                .add(pais)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d("FB","Paises agregado correctamente:" + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("FB","Error paises no agregado",e);
                    }
                });
    }


    public void onClickConsultarPais(View view) {
        db.collection("paises")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("FB", document.getId() +  "-->" + document.getData());
                            }
                        }
                    }
                });
    }

    public void onClickConsultarPaisPorId(View view) {
        DocumentReference prodRef = db.collection("paises").document("17asm4uDSGxAH0n4KHKG");
        prodRef
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            if (document.exists()) {
                                Log.d("FB", document.getData().toString());
                            }
                        }
                    }
                });
    }

    public void onClickModificar(View view) {
        PaisModelo p = new PaisModelo("Emiratos Árabes Unidos","9.770.529","https://www.youtube.com/watch?v=bGMgUIvuoKs","emiratosuni");
        String id = "17asm4uDSGxAH0n4KHKG";
        modificarPais(id,p);

        grabarPais();
    }
    private void modificarPais(String id, PaisModelo p) {
        crAmazon.document(id).set(p);
    }


    private void grabarPais() {
        PaisModelo p =  new PaisModelo("Singapur","5.703.600","https://www.youtube.com/watch?v=4eJK-A5Gl_4","singapur");
        crAmazon.document("500").set(p);
    }

    public void onClickCrear(View view) {
        //crearConDiccionario();
        PaisModelo p = new PaisModelo("Venezuela","32.219.521","https://www.youtube.com/watch?v=ls1E3-tXFBU","venezuela");
        crearPais(p);
    }
    //crear registros con pojo
    private void crearPais(PaisModelo p) {
        crAmazon
                .add(p)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d("FB","Pais agregado correctamente:" + documentReference.getId());
                    }
                });
    }
}
