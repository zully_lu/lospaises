package com.example.lospaises;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class FirebaseDatosActivity extends AppCompatActivity {
    EditText mEditTextPais;
    EditText mEditTextPoblacion;
    EditText mEditTextPaisVideo;
    EditText mEditTextPaisImagen;
    Button mButtonCrearDatos;

    FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firebase_datos);
        db = FirebaseFirestore.getInstance();

        mEditTextPais = findViewById(R.id.editTextPais);
        mEditTextPoblacion = findViewById(R.id.editTextPoblacion);
        mEditTextPaisVideo = findViewById(R.id.editTextVideo);
        mEditTextPaisImagen = findViewById(R.id.editTextImagen);
        mButtonCrearDatos = findViewById(R.id.btnCrearDatos);

        mButtonCrearDatos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                crearDatos();

            }
        });

    }
    private void crearDatos() {

        String pais = mEditTextPais.getText().toString();
        String poblacion = mEditTextPoblacion.getText().toString();
        String video = mEditTextPaisVideo.getText().toString();
        String imagen = mEditTextPaisImagen.getText().toString();


        Map<String,Object> map = new HashMap<>();
        map.put("pais",pais);
        map.put("poblacion", poblacion);
        map.put("video",video);
        map.put("imagen",imagen);

        db.collection("Paises")
                .add(map)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(FirebaseDatosActivity.this, "Paises agregado correctamente:", Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(FirebaseDatosActivity.this, "Error paises no agregado", Toast.LENGTH_SHORT).show();
                    }
                });

    }
}