package com.example.lospaises;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.util.List;

public class RecyclerViewAdaptador extends  RecyclerView.Adapter<RecyclerViewAdaptador.ViewHolder> {
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView pais, poblacion;
        ImageView fotoPais;
        WebView videoPais;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            pais = (TextView) itemView.findViewById(R.id.tvPais);
            poblacion = (TextView) itemView.findViewById(R.id.tvPoblacion);
            fotoPais = (ImageView) itemView.findViewById(R.id.imgPais);
            videoPais = (WebView) itemView.findViewById(R.id.videoPais);
        }
    }
    public List<PaisModelo> paisLista;

    public RecyclerViewAdaptador(Context context, List<PaisModelo> paisLista) {
        this.paisLista = paisLista;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_paises,parent,false);
        ViewHolder viewHolder= new ViewHolder(view);
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.pais.setText(paisLista.get(position).getPais());
        holder.poblacion.setText(paisLista.get(position).getPoblacion());
       /* holder.fotoPais.setImageResource(paisLista.get(position).getImgpais());*/
        holder.videoPais.loadUrl(paisLista.get(position).getVideopais());
        holder.videoPais.setWebViewClient(new WebViewClient());
        holder.videoPais.setWebChromeClient(new WebChromeClient());
        holder.videoPais.getSettings().setJavaScriptEnabled(true);


    }

    @Override
    public int getItemCount() {
        return paisLista.size();
    }
}
