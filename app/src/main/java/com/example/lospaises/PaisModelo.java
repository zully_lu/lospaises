package com.example.lospaises;


public class PaisModelo {
    private String pais, poblacion, videopais, imgpais;


    public PaisModelo() {
    }


    public PaisModelo(String pais, String poblacion,String videopais, String imgpais ) {
        this.pais = pais;
        this.poblacion = poblacion;
        this.videopais = videopais;
        this.imgpais = imgpais;


    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getPoblacion() {
        return poblacion;
    }

    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }
    public String getVideopais() {
        return videopais;
    }

    public void setVideopais(String videopais) {
        this.videopais = videopais;
    }

    public String getImgpais() {
        return imgpais;
    }

    public void setImgpais(String imgpais) {
        this.imgpais = imgpais;
    }


}


