package com.example.lospaises;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    FragmentTransaction transaction;
    Fragment fragmentInicio,fragmentoprimer, fragmentoSegundo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentInicio = new InicioFragment();
        fragmentoprimer = new PrimerFragment();
        fragmentoSegundo = new SegundoFragment();

        getSupportFragmentManager().beginTransaction().add(R.id.contenedorFragments, fragmentInicio ).commit();


    }
    public void OnClick (View view){
        transaction=getSupportFragmentManager().beginTransaction();
        switch (view.getId()) {
            case R.id.btnBandera: transaction.replace(R.id.contenedorFragments, fragmentoprimer);
                transaction.addToBackStack(null);
                break;
            case R.id.btnMapa: transaction.replace(R.id.contenedorFragments, fragmentoSegundo);
                transaction.addToBackStack(null);
                break;
        }
        transaction.commit();
    }
}
