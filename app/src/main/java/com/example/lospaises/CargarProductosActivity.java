package com.example.lospaises;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.example.lospaises.PaisModelo;
import com.example.lospaises.PaisCsv;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CargarProductosActivity extends AppCompatActivity {

    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cargar_productos);
        db = FirebaseFirestore.getInstance();
    }
    public void cargarDatosFirestorm(View view) throws IOException {
        List<PaisModelo> paises = new ArrayList<>();
        for (PaisModelo pais : paises) {
        Map<String,Object> mapa = getMapa(pais);
            db.collection("Paisess")
                    .add(pais)
                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            Toast.makeText(CargarProductosActivity.this, "Paises agregado correctamente:", Toast.LENGTH_SHORT).show();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(CargarProductosActivity.this, "Error paises no agregado", Toast.LENGTH_SHORT).show();
                }
            });

    }

}

    private Map<String, Object> getMapa(PaisModelo pais) {
        Map<String,Object> mapa = new HashMap<>();
        mapa.put("pais",pais.getPais());
        mapa.put("poblacion",pais.getPoblacion());
        mapa.put("video",pais.getVideopais());
        mapa.put("imagen",pais.getImgpais());
        return mapa;
    }
}
